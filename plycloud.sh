#!/bin/bash
# Author: Georgi 'Joe' Stanchev (joe@deploypi.com)
# Creation date: 29-Jun-2016
#
# Description: Master script to manage local server.
#
# Change log:
# 29-Jun-2016 - gstanchev - Initial Version
#

PS4='.+|${BASH_SOURCE##*/} ${LINENO}${FUNCNAME[0]:+ ${FUNCNAME[0]}}|'

if ((UID != 0 ))
    then
        echo "`date -u` - FATAL: This script must be executed with root user."
        exit 200
    fi

if ((BASH_VERSINFO[0] < 4))
    then
        echo "`date -u` - FATAL: Your version is '$BASH_VERSION', you need at least bash-4.0 to run this script."
        exit 201
    fi

if [[ ! -f ~/.plycloud ]]
    then
        echo "`date -u` - FATAL: Configuration file '~/.plycloud' do not exist. Please download your version from plycloud.com."
        exit 203
    fi

ERROR_MSG="Usage: $0 ( --bootstrap | --all | --configure | --run | --clear )

    -b|--bootstrap
        Install all prerequisites for plycloud.com

    -a|--all
        Clean all existing states, donwload latest version and run salt. Same as --clear --configure --run

    -g|--get-all
        Download all needed states and configurations from plyclod.com

    -r|--run
        Run salt and apply configs.

    -c|--clear
        Clean all salt states.

    -p|--get-pillar
        Get configuration pillar from plycloud.com

    -f|--force
        Force action.

    -d|--debug
        Turn on verbose mode.
"

if [ $# -eq 0 ]
    then
        echo "$ERROR_MSG"
        exit 250
    fi

PLYCLOUD_CLEAR="No"
PLYCLOUD_CONFIGURE="No"
PLYCLOUD_PILLAR="No"
PLYCLOUD_RUN="No"
PLYCLOUD_DEBUG="No"
PLYCLOUD_FORCE="No"

while [ $# -ge 1 ]
    do
        case $1 in
            -h|--help )
                echo "$ERROR_MSG"
                exit 0;;
            -b|--bootstrap )
                PLYCLOUD_BOOTSTRAP="Yes"
                shift ;;
            -a|--all )
                PLYCLOUD_CLEAR="Yes"
                PLYCLOUD_CONFIGURE="Yes"
                PLYCLOUD_PILLAR="Yes"
                PLYCLOUD_RUN="Yes"
                shift ;;
            -g|--get-all )
                PLYCLOUD_CLEAR="Yes"
                PLYCLOUD_CONFIGURE="Yes"
                PLYCLOUD_PILLAR="Yes"
                shift ;;
            -r|--run )
                PLYCLOUD_RUN="Yes"
                shift ;;
            -c|--clear )
                PLYCLOUD_CLEAR="Yes"
                shift ;;
            -p|--get-pillar )
                PLYCLOUD_PILLAR="Yes"
                shift ;;
            -d|--debug )
                PLYCLOUD_DEBUG="Yes"
                shift ;;
            -f|--force )
                PLYCLOUD_FORCE="Yes"
                shift ;;
            * ) shift ;;
        esac
    done

unset ERROR_MSG

########################################################################
export SCRIPT_DIRECTORY=$( cd "${0%/*}" && pwd -L )
export PLYCLOUD_CONFIG=~/.plycloud
. $PLYCLOUD_CONFIG
########################################################################

ply_get_pillar()
    {
        echo "`date -u` - INFO: Downloading pillar!"
        PLYCLOUD_OPTS="--silent"
        if [[ $PLYCLOUD_DEBUG == "Yes" ]]
            then
                unset PLYCLOUD_OPTS
            fi
        curl -k --fail $PLYCLOUD_OPTS --progress-bar \
                --output "$PLYCLOUD_SALT_PILLAR/plycloud.sls" \
                --header "Ply-Key: $PLYCLOUD_PLY_KEY" \
                --header "Ply-Secret: $PLYCLOUD_PLY_SECRET" \
                --header "Ply-HostName: $PLYCLOUD_PLY_HOSTNAME" \
                "$PLYCLOUD_INSTALL_CONFIG"
        PLY_ERR_STATUS=$?
        unset PLYCLOUD_OPTS
        if [[ $PLY_ERR_STATUS -ne 0 ]]
            then
                echo "`date -u` - ERROR: ($PLY_ERR_STATUS) Cannot download pillar! use -d for more info."
            fi
    }

ply_git_clone_salt_repo()
    {
        echo "`date -u` - INFO: Clone git repo..."
        git clone --depth 1 --single-branch --branch master $PLYCLOUD_GIT_URL /$PLYCLOUD_SALT_BASE/plycloud/
    }

ply_bootstrap()
    {
        echo "`date -u` - INFO: Bootstraping ..."
        # Activating ACT LED
        echo 1 > /sys/class/leds/led0/brightness
        echo default-on > /sys/class/leds/led0/trigger

        . /etc/*-release

        SALT_MINION_CONF="file_client: local
gitfs_provider: GitPython
fileserver_backend:
  - roots

file_roots:
  base:
    - $PLYCLOUD_SALT_BASE

pillar_roots:
  base:
    - $PLYCLOUD_SALT_PILLAR

log_level: warning
pillar_source_merging_strategy: recurse
"

        if [[ ! $( which salt-minion ) || $PLYCLOUD_FORCE == "Yes" ]]
            then
                if [[ "$ID" = "raspbian" ]]
                    then
                        apt-get -y update
                        apt-get -y install git curl python-git
                        curl -L https://bootstrap.saltstack.com | bash
                        service salt-minion stop
                        update-rc.d salt-minion disable
                        if [[ ! -f /usr/share/distro-info/raspbian.csv ]]
                            then
                            	# Most probably should be removed after official stretch release
                                cp /usr/share/distro-info/debian.csv /usr/share/distro-info/raspbian.csv
                            fi
                    fi
                mkdir -p $PLYCLOUD_SALT_BASE/plycloud/ $PLYCLOUD_SALT_PILLAR
                ply_git_clone_salt_repo
                if [[ ! -s /etc/salt/minion.d/deploypi_minion.conf || $PLYCLOUD_FORCE == "Yes" ]]
                    then
                        echo "`date -u` - INFO: Deployng new plycloud.com minion configuration ..."
                        echo "$SALT_MINION_CONF" > /etc/salt/minion.d/deploypi_minion.conf
                        echo "$PLYCLOUD_PLY_HOSTNAME" > /etc/salt/minion_id
                    fi
                if [[ ! -s /$PLYCLOUD_SALT_BASE/top.sls || $PLYCLOUD_FORCE == "Yes" ]]
                    then
                        echo "`date -u` - INFO: Deployng default salt top.sls ..."
                        cp -f /$PLYCLOUD_SALT_BASE/plycloud/top.sls /$PLYCLOUD_SALT_BASE/top.sls
                    fi
                if [[ ! -s /$PLYCLOUD_SALT_PILLAR/top.sls || $PLYCLOUD_FORCE == "Yes" ]]
                    then
                        echo "`date -u` - INFO: Deployng default pillar top.sls ..."
                        cp -f /$PLYCLOUD_SALT_BASE/plycloud/top.sls /$PLYCLOUD_SALT_PILLAR/top.sls
                    fi
                ply_get_pillar
            else
                echo "`date -u` - INFO: This server is already bootstrapped. You can force it."
            fi
        # De-activating ACT LED
        echo 0 > /sys/class/leds/led0/brightness
        echo mmc0 > /sys/class/leds/led0/trigger
    }

ply_clear_salt_dir()
    {
        echo "`date -u` - INFO: Removing existing salt states..."
        rm -rf /$PLYCLOUD_SALT_BASE/plycloud/
        mkdir -p /$PLYCLOUD_SALT_BASE/plycloud/
    }


ply_run_salt()
    {
        if [[ $PLYCLOUD_DEBUG == "Yes" ]]
            then
                PLYCLOUD_OPTS="-l debug"
            fi
        salt-call --local state.highstate $PLYCLOUD_OPTS
    }


########################################################################
echo "`date -u` - INFO: Starting the new job."
#######################################################################

if [[ $PLYCLOUD_BOOTSTRAP == "Yes" ]]
    then
        ply_bootstrap
        echo "`date -u` - INFO: Job finished."
        exit 0
    fi

if [[ $PLYCLOUD_CLEAR == "Yes" ]]
    then
        ply_clear_salt_dir
    fi

if [[ $PLYCLOUD_CONFIGURE == "Yes" ]]
    then
        ply_git_clone_salt_repo
    fi

if [[ $PLYCLOUD_PILLAR == "Yes" ]]
    then
        ply_get_pillar
    fi

if [[ $PLYCLOUD_RUN == "Yes" ]]
    then
        ply_run_salt
    fi
echo "`date -u` - INFO: Job finished"
