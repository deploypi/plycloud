# All-In-One script for bootstraping and applying plycloud.com configurations 

# sudo su -
# wget -N https://bitbucket.org/deploypi/plycloud/raw/master/plycloud.sh ; chmod 0755 plycloud.sh

Upload .plycloud configuration file to the device

# cat > .plycloud

Bootstrap it

# ./plycloud.sh --bootstrap

When configuration is ready on plycloud.com run

# ./plycloud.sh --run --get-pillar --debug

Reboot and enjoy yours new configuration

Usage: ./plycloud.sh ( --bootstrap | --all | --configure | --run | --clear )

	-b|--bootstrap
		Install all prerequisites for plycloud.com

	-a|--all
		Clean all existing states, donwload latest version and run salt. Same as --clear --configure --run

	-g|--get-all
		Download all needed states and configurations from plyclod.com

	-r|--run
		Run salt and apply configs.

	-c|--clear
		Clean all salt states.

	-p|--get-pillar
		Get configuration pillar from plycloud.com

	-f|--force
		Force action.

	-d|--debug

